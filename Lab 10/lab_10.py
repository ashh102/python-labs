import re

def split_line(line):
    return re.findall('[A-Za-z]+(?:\'[A-Za-z]+)?',line)



file = open("dictionary.txt")

dictionary_list = []
for line in file:
    line = line.strip()
    dictionary_list.append(line)
file.close()



# Linear.
print("--- Linear Search ---")

file = open("AliceInWonderland200.txt")

line_1 = 0
for line in file:
    line_1 += 1
    words = split_line(line)

    for word in words:
        key = word.upper()

        i = 0
        while i < len(dictionary_list) and dictionary_list[i] != key:
            i += 1
        if i >= len(dictionary_list):
            print("Line ", line_1, "Possible misspelled word: ", word)

file.close()



# Binary
print("--- Binary Search ---")

file = open("AliceInWonderland200.txt")

line_1 = 0
for line in file:
    line_1 += 1
    words = split_line(line)

    for word in words:
        key = word.upper()
        i = 0
        lower_bound = 0
        upper_bound = len(dictionary_list) - 1
        found = False

        while lower_bound <= upper_bound and not found:

            middle_pos = (lower_bound + upper_bound) // 2

            if dictionary_list[middle_pos] < key:
                lower_bound = middle_pos + 1
            elif dictionary_list[middle_pos] > key:
                upper_bound = middle_pos - 1
            else:
                found = True

        if not found:
            print("Line ", line_1, "Possible misspelled word: ", word)

file.close()





