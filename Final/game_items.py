import random
import arcade
import math


SPRITE_SCALING = 0.5
SCREEN_WIDTH = 1500
SCREEN_HEIGHT = 900
BULLET_SPEED = 5
TEXT = " GAME OVER"
TEXT1 = "You ran out of lives! Game over."
TEXT2 = "You won!"


class Alien2(arcade.Sprite):
    def __init__(self, filename, sprite_scaling):
        super().__init__(filename, sprite_scaling)
        self.change_y = 5
        self.change_x = 0

    def update(self):
        self.center_x += self.change_x
        self.center_y += self.change_y

        if self.left < 0:
            self.change_x *= -1
        if self.right > SCREEN_WIDTH:
            self.change_x *= -1
        if self.bottom < 0:
            self.change_y *= -1
        if self.top > SCREEN_HEIGHT:
            self.change_y *= -1


class Bullet(arcade.Sprite):
    def update(self):
        self.center_y += BULLET_SPEED + 15


class Asteroid(arcade.Sprite):
    def reset_pos(self):
        # Reset the asteroid to a random spot above the screen
        self.center_y = random.randrange(SCREEN_HEIGHT + 20, SCREEN_HEIGHT + 100)
        self.center_x = random.randrange(SCREEN_WIDTH)

    def update(self):
        # Move the asteroid
        self.center_y -= 7

        # See if the asteroid has fallen off the bottom of the screen.
        # If so, reset it.
        if self.top < 0:
            self.reset_pos()


class Pill(arcade.Sprite):
    def reset_pos(self):
        # Reset the coin to a random spot above the screen
        self.center_y = random.randrange(SCREEN_HEIGHT + 20, SCREEN_HEIGHT + 100)
        self.center_x = random.randrange(SCREEN_WIDTH)

    def update(self):
        # Move the pill
        self.center_y -= 7

        # See if the pill has fallen off the bottom of the screen.
        # If so, reset it.
        if self.top < 0:
            self.reset_pos()


class Star2(arcade.Sprite):
    def reset_pos(self):
        # Reset the star to a random spot above the screen
        self.center_y = random.randrange(SCREEN_HEIGHT + 20, SCREEN_HEIGHT + 100)
        self.center_x = random.randrange(SCREEN_WIDTH)

    def update(self):
        # Move the star
        self.center_y -= 6

        # See if the star has fallen off the bottom of the screen.
        # If so, reset it.
        if self.top < 0:
            self.reset_pos()


class Star3(arcade.Sprite):
    def __init__(self, filename, sprite_scaling):
        super().__init__(filename, sprite_scaling)

        # Current angle in radians
        self.angle = 0

        # How far away from the center to orbit, in pixels
        self.radius = 0

        # How fast to orbit, in radians per frame
        self.speed = 0.008

        # Set the center of the point we will orbit around
        self.circle_center_x = 0
        self.circle_center_y = 0

    def update(self):
        self.center_x = self.radius * math.sin(self.angle) \
            + self.circle_center_x
        self.center_y = self.radius * math.cos(self.angle) \
            + self.circle_center_y

        # Increase the angle in prep for the next round.
        self.angle += self.speed


class Alien3(arcade.Sprite):
    def reset_pos(self):
        # Reset the alien to a random spot above the screen
        self.center_y = random.randrange(SCREEN_HEIGHT + 20, SCREEN_HEIGHT + 100)
        self.center_x = random.randrange(SCREEN_WIDTH)

    def update(self):
        # Move the alien
        self.center_y -= 6

        # See if the alien has fallen off the bottom of the screen.
        # If so, reset it.
        if self.top < 0:
            self.kill()