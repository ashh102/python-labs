import arcade



SCREEN_WIDTH = 800
SCREEN_HEIGHT = 800
SQUARE_CENTER = 20
MOVEMENT_SPEED = 3



# These functions create the background.
def draw_grass():
    """
    This function draws grass.
    """
    arcade.draw_lrtb_rectangle_filled(0, 800, 150, 0, arcade.color.BRIGHT_GREEN)

def draw_mountain(position_x, position_y):
    """
    Draws mountain.
    """
    arcade.draw_triangle_filled(position_x, position_y,
                                position_x - 200, position_y + 200,
                                position_x - 400, position_y,
                                arcade.color.DONKEY_BROWN)


def draw_cloud(position_x, position_y):
    """
    Draws clouds.
    """
    arcade.draw_circle_filled(position_x, position_y,
                              75, arcade.color.LIGHT_SKY_BLUE)
    arcade.draw_circle_filled(position_x + 50, position_y - 20,
                              70, arcade.color.LIGHT_SKY_BLUE)
    arcade.draw_circle_filled(position_x + 25, position_y + 30,
                              70, arcade.color.LIGHT_SKY_BLUE)


def draw_tree(position_x, position_y):
    """
    This function draws a tree.
    """

    # Draws trunk of tree.
    arcade.draw_lrtb_rectangle_filled(position_x, position_y,
                              110, 0, arcade.color.PALE_BROWN)

    #Draws top of tree.
    arcade.draw_circle_filled(position_x + 10, 120, 50, arcade.color.NAPIER_GREEN)



# Ball class.
class Ball:
    def __init__(self, position_x, position_y, change_x, change_y, radius, color):

        self.position_x = position_x
        self.position_y = position_y
        self.change_x = change_x
        self.change_y = change_y
        self.radius = radius
        self.color = color
        self.sound = arcade.load_sound("lava.flac")


    def draw(self):
        arcade.draw_circle_filled(self.position_x, self.position_y, self.radius, self.color)


    def animate(self):
        # Moves the ball.
        self.position_y += self.change_y
        self.position_x += self.change_x

        # Limitations for movement at the edge of the screen.
        if self.position_x < self.radius:
            self.position_x = self.radius
            arcade.play_sound(self.sound)

        if self.position_x > SCREEN_WIDTH - self.radius:
            self.position_x = SCREEN_WIDTH - self.radius
            arcade.play_sound(self.sound)

        if self.position_y < self.radius:
            self.position_y = self.radius
            arcade.play_sound(self.sound)

        if self.position_y > SCREEN_HEIGHT - self.radius:
            self.position_y = SCREEN_HEIGHT - self.radius
            arcade.play_sound(self.sound)



# Square class.
class Square:

    def __init__(self, position_x, position_y,change_x, change_y, SQUARE_CENTER, color):
        self.position_x = position_x
        self.position_y = position_y
        self.change_x = change_x
        self.change_y = change_y
        self.SQUARE_CENTER = SQUARE_CENTER
        self.color = color
        self.sound = arcade.load_sound("lava.flac")


    def draw(self):
        arcade.draw_rectangle_filled(self.position_x, self.position_y, self.SQUARE_CENTER, self.SQUARE_CENTER, self.color)


    # Limitations for movement at the edge of the screen.
    def animate(self):
        if self.position_x < self.SQUARE_CENTER / 2:
            self.position_x = self.SQUARE_CENTER / 2
            arcade.play_sound(self.sound)

        if self.position_x > SCREEN_WIDTH - self.SQUARE_CENTER / 2:
            self.position_x = SCREEN_WIDTH - self.SQUARE_CENTER / 2
            arcade.play_sound(self.sound)

        if self.position_y < self.SQUARE_CENTER / 2:
            self.position_y = self.SQUARE_CENTER / 2
            arcade.play_sound(self.sound)

        if self.position_y > SCREEN_HEIGHT - self.SQUARE_CENTER / 2:
            self.position_y = SCREEN_HEIGHT - self.SQUARE_CENTER / 2
            arcade.play_sound(self.sound)



class MyApplication(arcade.Window):

    def __init__(self, width, height, title):
        super().__init__(width, height, title)

        self.set_mouse_visible(False)

        arcade.set_background_color(arcade.color.BLIZZARD_BLUE)

        # Creates ball.
        self.ball = Ball(50, 50, 0,0, 15, arcade.color.BRIGHT_PINK)

        # Creates square.
        self.square = Square(550,50,40,40,SQUARE_CENTER,arcade.color.BRIGHT_PINK)

        self.laser_sound = arcade.load_sound("laser.ogg")


    def animate(self, delta_time):
        self.ball.animate()
        self.square.animate()


    def on_key_press(self, key, modifiers):

        # Controls the ball with keyboard arrows.
        if key == arcade.key.LEFT:
            self.ball.change_x = -MOVEMENT_SPEED

        elif key == arcade.key.RIGHT:
            self.ball.change_x = MOVEMENT_SPEED

        elif key == arcade.key.UP:
            self.ball.change_y = MOVEMENT_SPEED

        elif key == arcade.key.DOWN:
            self.ball.change_y = -MOVEMENT_SPEED

        # Adds sound when keys are pressed.
        if key == arcade.key.DOWN:
            arcade.play_sound(self.laser_sound)

        if key == arcade.key.UP:
            arcade.play_sound(self.laser_sound)

        if key == arcade.key.LEFT:
            arcade.play_sound(self.laser_sound)

        if key == arcade.key.RIGHT:
            arcade.play_sound(self.laser_sound)


    def on_key_release(self, key, modifiers):

        if key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.ball.change_x = 0

        elif key == arcade.key.UP or key == arcade.key.DOWN:
            self.ball.change_y = 0


    def on_draw(self):
        arcade.start_render()
        draw_grass()
        draw_mountain(800, 150)
        draw_mountain(400, 150)
        draw_cloud(100, 600)
        draw_cloud(350, 550)
        draw_cloud(600, 500)
        draw_tree(100, 120)
        draw_tree(200, 220)
        draw_tree(300, 320)
        draw_tree(500, 520)
        draw_tree(600, 620)
        draw_tree(700, 720)
        self.ball.draw()
        self.square.draw()


    # Adds movement with mouse to the square.
    def on_mouse_motion(self, x, y, dx, dy):
        self.square.position_x = x
        self.square.position_y = y


    # Adds sound when mouse button is clicked.
    def on_mouse_press(self, x, y, button, modifiers):
        arcade.play_sound(self.laser_sound)


window = MyApplication(800, 800, "Lab 07")


arcade.run()