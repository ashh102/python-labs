# Rooms
room_list = []

room = ["You're in the living room.  The south hall is to the east, and the main bedroom is to the north.",
        3, 1, None, None]
room_list.append(room)

room = ["You're in the south hall.  The living room is to the west, the kitchen is to the east, "
        "and the north hall is to the north.", 4, 2, None, 0]
room_list.append(room)

room = ["You're in the kitchen.  The master bedroom is to the north, and the south hall is to the west.",
        5, None, None, 1]
room_list.append(room)

room = ["You're in the main bedroom.  The north hall is to the east, and the living room is to the south.",
        None, 4, 0, None]
room_list.append(room)

room = ["You're in the north hall.  The main bedroom is to the west, the balcony is to the north, "
        "the master bedroom is to the east, and the south hall is to the south.", 6, 5, 1, 3]
room_list.append(room)

room = ["You're in the master bedroom.  The north hall is to the west, and the kitchen is to the south.",
        None, None, 2, 4]
room_list.append(room)

room = ["You're out on the balcony.  Turn back.  You came from the south.", None, None, 4, None]
room_list.append(room)


# Starts the player in the living room.
current_room = 0


done = False


while not done:
    print(" ")
    print(room_list[current_room][0])
    user_input = input("What direction do you want to go in?")


    # North
    if user_input.upper() == "NORTH" or user_input.upper() == "N":
        next_room = room_list[current_room][1]
        # If there is no room to the North.
        if next_room is None:
            print(" ")
            print("You can't go that way.")
        # If there is a room to the North.
        else:
            current_room = next_room


    # South
    elif user_input.upper() == "SOUTH" or user_input.upper() == "S":
        next_room = room_list[current_room][3]
        # If there is no room to the South.
        if next_room is None:
            print(" ")
            print("You can't go that way.")
        # If there is a room to the South.
        else:
            current_room = next_room


    # East
    elif user_input.upper() == "EAST" or user_input.upper() == "E":
        next_room = room_list[current_room][2]
        # If there is no room to the East.
        if next_room is None:
            print(" ")
            print("You can't go that way.")
        # If there is a room to the East.
        else:
            current_room = next_room


    # West
    elif user_input.upper() == "WEST" or user_input.upper() == "W":
        next_room = room_list[current_room][4]
        # If there is no room to the West.
        if next_room is None:
            print(" ")
            print("You can't go that way.")
        # If there is a room to the West.
        else:
            current_room = next_room


    # If the player wants to quit.
    elif user_input.upper() == "QUIT" or user_input.upper() == "Q":
        print(" ")
        print("You quit. Game over.")
        done = True


    # Informs the player when they entered an invalid word or letter.
    else:
        print(" ")
        print("Error. Please type one of the following as your choice: north, south, east, west.")