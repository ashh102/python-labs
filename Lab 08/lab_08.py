import random
import arcade

SPRITE_SCALING = .5
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 800
TEXT = " Game over."


# Bone class.
class Bone(arcade.Sprite):

    def __init__(self, filename, sprite_scaling):
        super().__init__(filename, sprite_scaling)

        self.change_x = 0
        self.change_y = 0

    def update(self):
        self.center_x += self.change_x
        self.center_y += self.change_y

        if self.left < 0:
            self.change_x *= -1
        if self.right > SCREEN_WIDTH:
            self.change_x *= -1
        if self.bottom < 0:
            self.change_y *= -1
        if self.top > SCREEN_HEIGHT:
            self.change_y *= -1

# Collar class.
class Collar(arcade.Sprite):
    def __init__(self, filename, sprite_scaling):
        super().__init__(filename, sprite_scaling)
        self.change_y = -1

    def reset_pos(self):
        self.center_y = random.randrange(SCREEN_HEIGHT + 25, SCREEN_HEIGHT + 50)
        self.center_x = random.randrange(SCREEN_WIDTH)

    def update(self):
        self.center_y += self.change_y

        if self.top < 0:
            self.reset_pos()


class MyAppWindow(arcade.Window):
    def __init__(self, width, height):
        super().__init__(width, height)

        self.all_sprites_list = None
        self.bone_list = None
        self.collar_list = None
        self.player_sprite = None
        self.score = 0


    def start_new_game(self):
        # Sprite lists
        self.all_sprites_list = arcade.SpriteList()
        self.bone_list = arcade.SpriteList()
        self.collar_list = arcade.SpriteList()

        # Sets up player
        self.score = 0
        self.player_sprite = arcade.Sprite("download.png", SPRITE_SCALING / 2)
        self.player_sprite.center_x = 50
        self.player_sprite.center_y = 50
        self.all_sprites_list.append(self.player_sprite)

        for i in range(40):
            bone = Bone("bone-outlined-shape_318-75982.png", SPRITE_SCALING)

            # Positions the bone.
            bone.center_x = random.randrange(SCREEN_WIDTH)
            bone.center_y = random.randrange(SCREEN_HEIGHT)
            bone.change_x = random.randrange(-2, 2)
            bone.change_y = random.randrange(-2, 2)

            self.all_sprites_list.append(bone)
            self.bone_list.append(bone)

        for i in range(20):
            collar = Collar("download2.png", SPRITE_SCALING)

            # Positions the collar.
            collar.center_x = random.randrange(SCREEN_WIDTH)
            collar.center_y = random.randrange(SCREEN_HEIGHT)

            self.all_sprites_list.append(collar)
            self.collar_list.append(collar)

        self.set_mouse_visible(False)

        arcade.set_background_color(arcade.color.WHITE)


    def on_draw(self):
        arcade.start_render()
        self.all_sprites_list.draw()

        # Text for score.
        output = "Score: " + str(self.score)
        input = "Score: " + str(self.score) + TEXT

        if len(self.bone_list) > 0:
            arcade.draw_text(output, 10, 750, arcade.color.RED, 40)
        if len(self.bone_list) == 0:
            arcade.draw_text(input, 10, 750, arcade.color.RED, 40)
            Collar.change_y = 0


    # Controls player.
    def on_mouse_motion(self, x, y, dx, dy):
        if len(self.bone_list) > 0:
            self.player_sprite.center_x = x
            self.player_sprite.center_y = y


    def animate(self, delta_time):
        if len(self.collar_list) > 0:
            self.all_sprites_list.update()

        # Checks for collision between player and collars.
        hit_list1 = arcade.check_for_collision_with_list(self.player_sprite, self.collar_list)
        for collar in hit_list1:
            self.score -= 1
            collar.kill()

        # Checks for collision between player and bones.
        hit_list2 = arcade.check_for_collision_with_list(self.player_sprite, self.bone_list)
        for bone in hit_list2:
            self.score += 1
            bone.kill()


def main():
    window = MyAppWindow(SCREEN_WIDTH, SCREEN_HEIGHT)
    window.start_new_game()
    arcade.run()

if __name__ == "__main__":
    main()