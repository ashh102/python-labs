import random


# Prints directions at the beginning of the game.
def main():
    print("Welcome to Camel!")
    print("You're a camel, and you escaped a zoo.")
    print("The zookeepers want you back.")
    print("Out run the zookeepers and make it to the end of the city to be free.")


    # Sets variables and their starting values.
    miles_traveled = 0
    thirst = 0
    camel_tiredness = 0
    zookeepers_distance = -20
    drinks_from_puddles = 5


    print(" ")
    # Prints player's choices.
    done = False
    while done == False:
        print("A. Stop and get a drink of water from a puddle.")
        print("B. Run ahead at moderate speed.")
        print("C. Run ahead at full speed.")
        print("D. Stop and take a nap.")
        print("E. Status check.")
        print("Q. Quit.")


        print(" ")
        # Asks the player for their choice.
        user_input = input("What is your choice? ")

        # Player chooses to quit.
        if user_input.upper() == "Q":
            done = True

        # Player chooses status check.
        elif user_input.upper() == "E":
            print("Zookeepers are", miles_traveled - zookeepers_distance, "miles behind you.")

        # Player chooses to stop and nap.
        elif user_input.upper() == "D":
            print("You're rested and happy.")
            zookeepers_distance = zookeepers_distance + random.randrange(5, 21)
            print("Zookeepers distance from you: ", miles_traveled - zookeepers_distance)
            camel_tiredness = 0

        # Player chooses to run at full speed.
        elif user_input.upper() == "C":
            x = random.randrange(13, 18)
            z = random.randrange(1, 21)
            print("Runs", x)
            thirst = thirst + 1
            camel_tiredness = camel_tiredness + random.randrange(1, 4)
            zookeepers_distance = zookeepers_distance + random.randrange(7, 14)
            miles_traveled = miles_traveled + x
            # Provides the equivalent to an oasis.
            if z == 1:
                print("You've found a pond!  You're rested and no longer thirsty.")
                thirst = 0
                camel_tiredness = 0
                drinks_from_puddles = 5

        # Player chooses to run at moderate speed.
        elif user_input.upper() == "B":
            y = random.randrange(5, 11)
            print("Runs", y)
            thirst = thirst + 1
            camel_tiredness = camel_tiredness + 1
            zookeepers_distance = zookeepers_distance + random.randrange(10, 16)
            miles_traveled = miles_traveled + y

        # Player chooses to stop and drink.
        elif user_input.upper() == "A":
            if drinks_from_puddles == 0:
                w = random.randrange(1, 4)
                print("Oh no! All of the water has dried up!")
                zookeepers_distance = zookeepers_distance + w
            if drinks_from_puddles > 0:
                w = random.randrange(1, 4)
                print("You have taken a drink from a puddle.")
                zookeepers_distance = zookeepers_distance + w
                thirst = 0
                drinks_from_puddles = drinks_from_puddles - 1


        # Ends the game if the player gets too tired.
        if camel_tiredness > 8:
            print("You died of exhaustion!")
            done = True

        # Ends the game if the player is too thirsty.
        elif thirst >= 6:
            print("You died of thirst!")
            done = True

        # Ends the game when the zookeepers catch up to the player.
        elif miles_traveled - zookeepers_distance <= 0:
            print("You've been caught!  Game over.")
            done = True

        # Informs the player when they need to drink.
        if not done and (thirst == 4 or thirst == 5):
            print("You are thirsty!")

        # Informs the player when they need rest.
        if not done and (camel_tiredness >= 5 and camel_tiredness <= 7):
            print("You're getting tired.")

        # Informs player when the zookeepers are less than 10 miles away from them.
        if not done and (0 < miles_traveled - zookeepers_distance < 10):
            print("The zookeepers are getting close.")

        # Player wins if they travel 200+ miles without being caught.
        if miles_traveled > 199 and zookeepers_distance <= 199:
            print("You're free!  You won!")
            done = True


        # Prints a space between the feedback given and the new answer choices.
        print(" ")


# Calls the main function.
main()