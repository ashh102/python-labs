import arcade


# Sets the screen size.
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 800



# Sets the size of the circle.
CIRC_WIDTH = 50
CIRC_HEIGHT = 50


def on_draw(delta_time):
    """
    Draws everything to the screen.
    """

    arcade.start_render()

    # Draws landscape.
    draw_sun(on_draw.center_x, on_draw.center_y)
    draw_grass()
    draw_mountain(800, 150)
    draw_mountain(400, 150)
    draw_cloud(100, 600)
    draw_cloud(350, 550)
    draw_cloud(600, 500)
    draw_tree(100, 120)
    draw_tree(200, 220)
    draw_tree(300, 320)
    draw_tree(500, 520)
    draw_tree(600, 620)
    draw_tree(700, 720)



    # Modifies circle's position based on delta vector.
    on_draw.center_x += on_draw.delta_x * delta_time
    on_draw.center_y += on_draw.delta_y * delta_time

    # Tells where to reverse.
    if on_draw.center_x < CIRC_WIDTH // 2 \
            or on_draw.center_x > SCREEN_WIDTH - CIRC_WIDTH // 2:
        on_draw.delta_x *= -1
    if on_draw.center_y < CIRC_HEIGHT // 2 \
            or on_draw.center_y > SCREEN_HEIGHT - CIRC_HEIGHT // 2:
        on_draw.delta_y *= -1



def draw_grass():
    """
    This function draws grass.
    """
    arcade.draw_lrtb_rectangle_filled(0, 800, 150, 0, arcade.color.BRIGHT_GREEN)



def draw_sun(position_x, position_y):
    """
    This function draws the sun.
    """

    # Draws the circle for the sun
    arcade.draw_circle_filled(position_x, position_y, 50, arcade.color.ELECTRIC_YELLOW)

    # Draws lines for rays of sun.
    arcade.draw_line(position_x, position_y,
                     position_x, position_y + 100,
                     arcade.color.ELECTRIC_YELLOW, 2)
    arcade.draw_line(position_x, position_y,
                     position_x - 80, position_y + 60,
                     arcade.color.ELECTRIC_YELLOW, 2)
    arcade.draw_line(position_x, position_y,
                     position_x + 80, position_y + 60,
                     arcade.color.ELECTRIC_YELLOW, 2)
    arcade.draw_line(position_x, position_y,
                     position_x, position_y - 100,
                     arcade.color.ELECTRIC_YELLOW, 2)
    arcade.draw_line(position_x, position_y,
                     position_x - 80, position_y - 60,
                     arcade.color.ELECTRIC_YELLOW, 2)
    arcade.draw_line(position_x, position_y,
                     position_x + 80, position_y - 60,
                     arcade.color.ELECTRIC_YELLOW, 2)



def draw_mountain(position_x, position_y):
    """
    Draws mountain.
    """
    arcade.draw_triangle_filled(position_x, position_y,
                                position_x - 200, position_y + 200,
                                position_x - 400, position_y,
                                arcade.color.DONKEY_BROWN)



def draw_cloud(position_x, position_y):
    """
    Draws clouds.
    """
    arcade.draw_circle_filled(position_x, position_y,
                              75, arcade.color.LIGHT_SKY_BLUE)
    arcade.draw_circle_filled(position_x + 50, position_y - 20,
                              70, arcade.color.LIGHT_SKY_BLUE)
    arcade.draw_circle_filled(position_x + 25, position_y + 30,
                              70, arcade.color.LIGHT_SKY_BLUE)



def draw_tree(position_x, position_y):
    """
    This function draws a tree.
    """

    # Draws trunk of tree.
    arcade.draw_lrtb_rectangle_filled(position_x, position_y,
                              110, 0, arcade.color.PALE_BROWN)

    #Draws top of tree.
    arcade.draw_circle_filled(position_x + 10, 120, 50, arcade.color.NAPIER_GREEN)








def main():
    """
    Main function that's called to run program.
    """
    arcade.open_window("My Drawing with Functions; Lab 03", 800, 800)
    arcade.set_background_color(arcade.color.BLIZZARD_BLUE)
    arcade.start_render()

    # Tells what time interval to draw on.
    arcade.schedule(on_draw, 1 / 100)



    # Initial x and y position.
    on_draw.center_x = 100
    on_draw.center_y = 100

    # Initial changes in x and y (makes the circle go from bottom left corner
    # to the top right corner in an even fashion).
    on_draw.delta_x = 150
    on_draw.delta_y = 150

    arcade.finish_render()
    arcade.run()

if __name__ == "__main__":
    main()



