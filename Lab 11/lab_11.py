import arcade


ROW_COUNT = 10
COLUMN_COUNT = 10


WIDTH = 20
HEIGHT = 20


MARGIN = 5


SCREEN_WIDTH = (WIDTH + MARGIN) * COLUMN_COUNT + MARGIN
SCREEN_HEIGHT = (HEIGHT + MARGIN) * ROW_COUNT + MARGIN


class MyApplication(arcade.Window):

    def __init__(self, width, height):
        super().__init__(width, height)

        self.grid = []

        for row in range(ROW_COUNT):
            self.grid.append([])

            for column in range(COLUMN_COUNT):
                self.grid[row].append(0)

        arcade.set_background_color(arcade.color.BLACK)

    def on_draw(self):

        arcade.start_render()

        # Draws grid.
        for row in range(ROW_COUNT):
            for column in range(COLUMN_COUNT):

                if self.grid[row][column] == 1:
                    color = arcade.color.GREEN
                else:
                    color = arcade.color.WHITE

                x = (MARGIN + WIDTH) * column + MARGIN + WIDTH // 2
                y = (MARGIN + HEIGHT) * row + MARGIN + HEIGHT // 2

                # Draws box.
                arcade.draw_rectangle_filled(x, y, WIDTH, HEIGHT, color)

    def on_mouse_press(self, x, y, button, modifiers):

        column = x // (WIDTH + MARGIN)
        row = y // (HEIGHT + MARGIN)

        print("Click coordinates: ({}, {}). Grid coordinates: ({}, {})".format(x, y, row, column))

        # Flips color for center square.
        if row < ROW_COUNT and column < COLUMN_COUNT:
            if self.grid[row][column] == 0:
                self.grid[row][column] = 1
            else:
                self.grid[row][column] = 0

        # Flips color for right square.
        if row + 1 < ROW_COUNT:
            if self.grid[row + 1][column] == 0:
                self.grid[row + 1][column] = 1
            else:
                self.grid[row + 1][column] = 0

        # Flips color for left square.
        if row - 1 >= 0:
            if self.grid[row - 1][column] == 0:
                self.grid[row - 1][column] = 1
            else:
                self.grid[row - 1][column] = 0

        # Flips color for top square.
        if column + 1 < COLUMN_COUNT:
            if self.grid[row][column + 1] == 0:
                self.grid[row][column + 1] = 1
            else:
                self.grid[row][column + 1] = 0

        # Flips color for bottom square.
        if column - 1 >= 0:
            if self.grid[row][column - 1] == 0:
                self.grid[row][column - 1] = 1
            else:
                self.grid[row][column - 1] = 0


window = MyApplication(SCREEN_WIDTH, SCREEN_HEIGHT)

arcade.run()