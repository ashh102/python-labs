import arcade
arcade.open_window("My Drawing", 800, 800)

# Setting the background color.
arcade.set_background_color(arcade.color.BLIZZARD_BLUE)

arcade.start_render()

# -------Drawing sun-------

# Drawing circle for center of sun.
arcade.draw_circle_filled(400, 200, 50, arcade.color.ELECTRIC_YELLOW)

# Drawing lines for rays of sun.
arcade.draw_line(400, 200, 400, 300, arcade.color.ELECTRIC_YELLOW, 2)
arcade.draw_line(400, 200, 320, 260, arcade.color.ELECTRIC_YELLOW, 2)
arcade.draw_line(400, 200, 480, 260, arcade.color.ELECTRIC_YELLOW, 2)



# Drawing grass.
arcade.draw_lrtb_rectangle_filled(0, 800, 150, 0, arcade.color.BRIGHT_GREEN)



# Drawing mountains.
arcade.draw_triangle_filled(800, 150, 600, 350, 400, 150, arcade.color.DONKEY_BROWN)
arcade.draw_triangle_filled(400, 150, 200, 350, 0, 150, arcade.color.DONKEY_BROWN)



# Drawing left cloud.
arcade.draw_circle_filled(100, 600, 75, arcade.color.LIGHT_SKY_BLUE)
arcade.draw_circle_filled(150, 580, 70, arcade.color.LIGHT_SKY_BLUE)
arcade.draw_circle_filled(125, 630, 70, arcade.color.LIGHT_SKY_BLUE)



# Drawing right cloud.
arcade.draw_circle_filled(600, 500, 75, arcade.color.LIGHT_SKY_BLUE)
arcade.draw_circle_filled(650, 480, 70, arcade.color.LIGHT_SKY_BLUE)
arcade.draw_circle_filled(625, 530, 70, arcade.color.LIGHT_SKY_BLUE)



# -------Drawing trees-------

# Drawing tree trunks.
arcade.draw_lrtb_rectangle_filled(100, 120, 110, 0, arcade.color.PALE_BROWN)
arcade.draw_lrtb_rectangle_filled(200, 220, 110, 0, arcade.color.PALE_BROWN)
arcade.draw_lrtb_rectangle_filled(300, 320, 110, 0, arcade.color.PALE_BROWN)

# Drawing tops of trees.
arcade.draw_circle_filled(110, 120, 50, arcade.color.NAPIER_GREEN)
arcade.draw_circle_filled(210, 120, 50, arcade.color.NAPIER_GREEN)
arcade.draw_circle_filled(310, 120, 50, arcade.color.NAPIER_GREEN)



# -------Drawing house-------

# Drawing base of the house.
arcade.draw_lrtb_rectangle_filled(500, 650, 120, 20, arcade.color.PALATINATE_PURPLE)

# Drawing roof of the house.
arcade.draw_triangle_filled(500, 120, 575, 180, 650, 120, arcade.color.PALE_COPPER)

# Drawing door on house.
arcade.draw_lrtb_rectangle_filled(560, 590, 50, 20, arcade.color.OU_CRIMSON_RED)

# Drawing left window on the house.
arcade.draw_lrtb_rectangle_filled(520, 540, 80, 40, arcade.color.WHITE)

# Drawing right window on the house.
arcade.draw_lrtb_rectangle_filled(610, 630, 80, 40, arcade.color.WHITE)


arcade.finish_render()
arcade.run()



