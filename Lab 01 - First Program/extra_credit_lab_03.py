import arcade


# Sets the screen size.
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 800


# Sets the size of the circle.
CIRC_WIDTH = 50
CIRC_HEIGHT = 50


def on_draw(delta_time):
    """
    Draws everything to the screen.
    """

    arcade.start_render()

    # Draws circle.
    arcade.draw_circle_filled(on_draw.center_x, on_draw.center_y, 25, arcade.color.BABY_BLUE)

    # Modifies circle's position based on delta vector.
    on_draw.center_x += on_draw.delta_x * delta_time
    on_draw.center_y += on_draw.delta_y * delta_time

    # Tells where to reverse.
    if on_draw.center_x < CIRC_WIDTH // 2 \
            or on_draw.center_x > SCREEN_WIDTH - CIRC_WIDTH // 2:
        on_draw.delta_x *= -1
    if on_draw.center_y < CIRC_HEIGHT // 2 \
            or on_draw.center_y > SCREEN_HEIGHT - CIRC_HEIGHT // 2:
        on_draw.delta_y *= -1


# Initial x and y position.
on_draw.center_x = 100
on_draw.center_y = 100

# Initial changes in x and y (makes the circle go from bottom left corner
# to the top right corner in an even fashion).
on_draw.delta_x = 150
on_draw.delta_y = 150


def main():
    # Opens window.
    arcade.open_window("Bouncing Circle", SCREEN_WIDTH, SCREEN_HEIGHT)
    arcade.set_background_color(arcade.color.BLACK)

    # Tells what time interval to draw on.
    arcade.schedule(on_draw, 1 / 100)

    # Runs program.
    arcade.run()


if __name__ == "__main__":
    main()