import arcade


SPRITE_SCALING = 0.25
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 800
MOVEMENT_SPEED = 5
TEXT = " GAME OVER"


class MyApplication(arcade.Window):

    def __init__(self, width, height):
        super().__init__(width, height)

        self.all_sprites_list = None
        self.coin_list = None
        self.laser_sound = arcade.load_sound("laser.ogg")

        # Set up the player
        self.score = 0
        self.player_sprite = None
        self.wall_list = None

        self.physics_engine = None



    def setup(self):

        # Sprite lists
        self.all_sprites_list = arcade.SpriteList()
        self.wall_list = arcade.SpriteList()
        self.coin_list = arcade.SpriteList()


        # Sets up the player.
        self.score = 0
        self.player_sprite = arcade.Sprite("player_22.png", SPRITE_SCALING * 2)
        self.player_sprite.center_x = 100
        self.player_sprite.center_y = 70
        self.all_sprites_list.append(self.player_sprite)


        # Walls along the sides of the screen.
        for x in range(15, 795, 35):
            wall = arcade.Sprite("crate_09.png", SPRITE_SCALING)
            wall.center_x = x
            wall.center_y = 780
            self.all_sprites_list.append(wall)
            self.wall_list.append(wall)

        for x in range(15, 795, 35):
            wall = arcade.Sprite("crate_09.png", SPRITE_SCALING)
            wall.center_x = x
            wall.center_y = 20
            self.all_sprites_list.append(wall)
            self.wall_list.append(wall)

        for y in range(20, 780, 40):
            wall = arcade.Sprite("crate_09.png", SPRITE_SCALING)
            wall.center_x = 785
            wall.center_y = y
            self.all_sprites_list.append(wall)
            self.wall_list.append(wall)

        for y in range(20, 780, 40):
            wall = arcade.Sprite("crate_09.png", SPRITE_SCALING)
            wall.center_x = 15
            wall.center_y = y
            self.all_sprites_list.append(wall)
            self.wall_list.append(wall)


        # Creates walls to go around.
        for x in range(55, 300, 35):
            wall = arcade.Sprite("crate_07.png", SPRITE_SCALING)
            wall.center_x = x
            wall.center_y = 150
            self.all_sprites_list.append(wall)
            self.wall_list.append(wall)

        for x in range(430, 765, 35):
            wall = arcade.Sprite("crate_07.png", SPRITE_SCALING)
            wall.center_x = x
            wall.center_y = 150
            self.all_sprites_list.append(wall)
            self.wall_list.append(wall)

        for x in range(55, 500, 35):
            wall = arcade.Sprite("crate_10.png", SPRITE_SCALING)
            wall.center_x = x
            wall.center_y = 300
            self.all_sprites_list.append(wall)
            self.wall_list.append(wall)

        for y in range(300, 500, 35):
            wall = arcade.Sprite("crate_08.png", SPRITE_SCALING)
            wall.center_x = 625
            wall.center_y = y
            self.all_sprites_list.append(wall)
            self.wall_list.append(wall)

        for x in range(625, 780, 40):
            wall = arcade.Sprite("crate_08.png", SPRITE_SCALING)
            wall.center_x = x
            wall.center_y = 475
            self.all_sprites_list.append(wall)
            self.wall_list.append(wall)

        for x in range(150, 500, 35):
            wall = arcade.Sprite("crate_11.png", SPRITE_SCALING)
            wall.center_x = x
            wall.center_y = 430
            self.all_sprites_list.append(wall)
            self.wall_list.append(wall)

        for y in range(430, 750, 35):
            wall = arcade.Sprite("crate_11.png", SPRITE_SCALING)
            wall.center_x = 150
            wall.center_y = y
            self.all_sprites_list.append(wall)
            self.wall_list.append(wall)

        for x in range(300, 650, 35):
            wall = arcade.Sprite("crate_12.png", SPRITE_SCALING)
            wall.center_x = x
            wall.center_y = 650
            self.all_sprites_list.append(wall)
            self.wall_list.append(wall)


        # Creates coins
        for x in range(300, 625, 30):
            coin = arcade.Sprite("environment_12.png", SPRITE_SCALING)
            coin.center_x = x
            coin.center_y = 575
            self.all_sprites_list.append(coin)
            self.coin_list.append(coin)

        for x in range(300, 625, 30):
            coin = arcade.Sprite("environment_12.png", SPRITE_SCALING)
            coin.center_x = x
            coin.center_y = 715
            self.all_sprites_list.append(coin)
            self.coin_list.append(coin)

        for y in range(480, 700, 60):
            coin = arcade.Sprite("environment_12.png", SPRITE_SCALING)
            coin.center_x = 200
            coin.center_y = y
            self.all_sprites_list.append(coin)
            self.coin_list.append(coin)

        for y in range(540, 760, 60):
            coin = arcade.Sprite("environment_12.png", SPRITE_SCALING)
            coin.center_x = 250
            coin.center_y = y
            self.all_sprites_list.append(coin)
            self.coin_list.append(coin)

        for y in range(540, 750, 60):
            coin = arcade.Sprite("environment_12.png", SPRITE_SCALING)
            coin.center_x = 725
            coin.center_y = y
            self.all_sprites_list.append(coin)
            self.coin_list.append(coin)

        for y in range(570, 720, 60):
            coin = arcade.Sprite("environment_12.png", SPRITE_SCALING)
            coin.center_x = 675
            coin.center_y = y
            self.all_sprites_list.append(coin)
            self.coin_list.append(coin)

        for x in range(450, 750, 30):
            coin = arcade.Sprite("environment_12.png", SPRITE_SCALING)
            coin.center_x = x
            coin.center_y = 90
            self.all_sprites_list.append(coin)
            self.coin_list.append(coin)

        for x in range(75, 250, 30):
            coin = arcade.Sprite("environment_12.png", SPRITE_SCALING)
            coin.center_x = x
            coin.center_y = 225
            self.all_sprites_list.append(coin)
            self.coin_list.append(coin)

        for x in range(75, 450, 30):
            coin = arcade.Sprite("environment_12.png", SPRITE_SCALING)
            coin.center_x = x
            coin.center_y = 370
            self.all_sprites_list.append(coin)
            self.coin_list.append(coin)

        for y in range(300, 450, 60):
            coin = arcade.Sprite("environment_12.png", SPRITE_SCALING)
            coin.center_x = 700
            coin.center_y = y
            self.all_sprites_list.append(coin)
            self.coin_list.append(coin)

        for y in range(440, 760, 60):
            coin = arcade.Sprite("environment_12.png", SPRITE_SCALING)
            coin.center_x = 60
            coin.center_y = y
            self.all_sprites_list.append(coin)
            self.coin_list.append(coin)

        for y in range(440, 760, 60):
            coin = arcade.Sprite("environment_12.png", SPRITE_SCALING)
            coin.center_x = 110
            coin.center_y = y
            self.all_sprites_list.append(coin)
            self.coin_list.append(coin)


        # Creates the physics engine.
        self.physics_engine = arcade.PhysicsEngineSimple(self.player_sprite, self.wall_list)


        # Sets background color.
        arcade.set_background_color(arcade.color.BLACK)



    def on_draw(self):

        arcade.start_render()
        self.all_sprites_list.draw()

        # Score.
        output = "Score: " + str(self.score)
        input = "Score: " + str(self.score) + TEXT

        if len(self.coin_list) > 0:
            arcade.draw_text(output, 0, 5, arcade.color.WHITE, 40)
        if len(self.coin_list) == 0:
            arcade.draw_text(input, 0, 5, arcade.color.WHITE, 40)



    def on_key_press(self, key, modifiers):

        if key == arcade.key.UP:
            self.player_sprite.change_y = MOVEMENT_SPEED
        elif key == arcade.key.DOWN:
            self.player_sprite.change_y = -MOVEMENT_SPEED
        elif key == arcade.key.LEFT:
            self.player_sprite.change_x = -MOVEMENT_SPEED
        elif key == arcade.key.RIGHT:
            self.player_sprite.change_x = MOVEMENT_SPEED



    def on_key_release(self, key, modifiers):

        if key == arcade.key.UP or key == arcade.key.DOWN:
            self.player_sprite.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.player_sprite.change_x = 0



    def animate(self, delta_time):

        self.physics_engine.update()

        # Coins collected by player.
        hit_list = arcade.check_for_collision_with_list(self.player_sprite, self.coin_list)
        for coin in hit_list:
            self.score += 1
            coin.kill()
            arcade.play_sound(self.laser_sound)



window = MyApplication(SCREEN_WIDTH, SCREEN_HEIGHT)
window.setup()
arcade.run()