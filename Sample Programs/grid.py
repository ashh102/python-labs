import arcade

SCREEN_WIDTH = 255
SCREEN_HEIGHT = 255

WIDTH = 20
HEIGHT = 20
MARGIN = 5


class MyApplication(arcade.Window):
    """
    Main application class.

    NOTE: Go ahead and delete the methods you don't need.
    If you do need a method, delete the 'pass' and replace it
    with your own code. Don't leave 'pass' in this program.
    """

    def __init__(self, width, height):
        super().__init__(width, height)


        arcade.set_background_color(arcade.color.BLACK)

        self.grid = []
        for row in range(10):
            # Add an empty row.
            self.grid.append([])
            for column in range(10):
                self.grid[row].append(0)

        self.grid[2][2] = 1
        print(self.grid)

    def on_draw(self):
        """
        Render the screen.
        """

        # This command should happen before we start drawing. It will clear
        # the screen to the background color, and erase what we drew last frame.
        arcade.start_render()

        for row in range(10):
            for column in range(10):
                if self.grid[row][column] == 1:
                    color = arcade.color.GREEN
                else:
                    color = arcade.color.WHITE

                arcade.draw_rectangle_filled(column * (WIDTH + MARGIN)+ WIDTH // 2 + MARGIN,
                                             row * (HEIGHT + MARGIN) + HEIGHT // 2 + MARGIN,
                                             WIDTH, HEIGHT, color)


    def on_mouse_press(self, x, y, button, key_modifiers):
        """
        Called when the user presses a mouse button.
        """
        print(x // (WIDTH + MARGIN), y // (HEIGHT + MARGIN))


        self.grid[y // HEIGHT + MARGIN][x // WIDTH + MARGIN] = 1



window = MyApplication(SCREEN_WIDTH, SCREEN_HEIGHT)

arcade.run()