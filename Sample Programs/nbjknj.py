import random
import arcade

SPRITE_SCALING = .5
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 800


class Bone(arcade.Sprite):
    def __init__(self, filename, sprite_scaling):
        super().__init__(filename, sprite_scaling)

        self.change_x = 0
        self.change_y = 0

        def update(self):
            self.center_x += self.change_x
            self.center_y += self.change_y

        if self.left < 0:
            self.change_x *= -1
        if self.right > SCREEN_WIDTH:
            self.change_x *= -1
        if self.bottom < 0:
            self.change_y *= -1
        if self.top > SCREEN_HEIGHT:
            self.change_y *= -1


class Collar(arcade.Sprite):
    def __init__(self, filename, sprite_scaling):
        super().__init__(filename, sprite_scaling)
        def update(self):
            self.center_y -= 1

            if self.top < -50:
                self.center_y = random.randrange(SCREEN_HEIGHT + 50, SCREEN_HEIGHT + 100)
                self.center_x = random.randrange(SCREEN_WIDTH)


class MyAppWindow(arcade.Window):
    def __init__(self, width, height):
        super().__init__(width, height)

        self.all_sprites_list = None
        self.bone_list = None
        self.collar_list = None

        self.player_sprite = None
        self.score = 0

        self.set_mouse_visible(False)

        arcade.set_background_color(arcade.color.BLACK)

    def setup(self):
        # Sprite lists
        self.all_sprites_list = arcade.SpriteList()
        self.bone_list = arcade.SpriteList()

        # Sets up player
        self.score = 0
        self.player_sprite = arcade.Sprite("download.png", SPRITE_SCALING / 2)
        self.player_sprite.center_x = 50
        self.player_sprite.center_y = 50
        self.all_sprites_list.append(self.player_sprite)

        for i in range(30):
            bone = Bone("bone-outlined-shape_318-75982.png", SPRITE_SCALING / 3)

            # Positions the bone
            bone.center_x = random.randrange(SCREEN_WIDTH)
            bone.center_y = random.randrange(SCREEN_HEIGHT)

            self.all_sprites_list.append(bone)
            self.bone_list.append(bone)

        for i in range(50):
            collar = Collar("download2.png", SPRITE_SCALING / 3)

            collar.center_x = random.randrange(SCREEN_WIDTH)
            collar.center_y = random.randrange(SCREEN_HEIGHT)

            self.all_sprites_list.append(collar)
            self.collar_list.append(collar)

    def on_draw(self):
        arcade.start_render()
        self.all_sprites_list.draw()

        output = "Score:" + str(self.score)
        arcade.draw_text(output, 10, 20, arcade.color.WHITE, 14)

    def on_mouse_motion(self, x, y, dx, dy):
        self.player_sprite.center_x = x
        self.player_sprite.center_y = y

    def animate(self, delta_time):
        self.all_sprites_list.update()
        hit_list = arcade.check_for_collision_with_list(self.player_sprite, self.collar_list)
        for collar in hit_list:
            self.score -= 1
            collar.center_y = random.randrange(SCREEN_HEIGHT + 50, SCREEN_HEIGHT + 100)
            collar.center_x = random.randrange(SCREEN_WIDTH)

        hit_list = arcade.check_for_collision_with_list(self.player_sprite, self.bone_list)
        for bone in hit_list:
            self.score += 1
            bone.kill()


def main():
    window = MyAppWindow(SCREEN_WIDTH, SCREEN_HEIGHT)
    window.setup()
    arcade.run()


main()