# If you do not have quotes in the parentheses, it's treated as a function, and it solves for x.
x = 5 + 10
print(x)

# "=" is an assignment operator.
# all the symbols in the expressions are called operators.
# "//" is called integer division.  Divides but always rounds down.
# "%" is the modulus operator.  This gives you the remainder value when dividing.
# Always put spaces around operators.
# You always need operators.  x = 5 and y = 2x will not work.  You need to write y = 2 * x.
# If defining a constant, always use uppercase letters.




def f():
    x = 22

# If you type "print(x)", x is not defined.


# Create the variable x and set it to 44.
x = 44

# Define a simple function that prints x.
def f():
    print(x)
# This actually works because x was not originally defined in a function.  If the variable is defined outside of a function, then it can be used globally.




def a():
    print("A")

a()
# This will print A.


