for
    # Know how many times.
while
    # Doesn't know how many times.

for i in range(5):
    print("I will not chew gum in class.")
    # This will print the statement 5 times

for i in range(5):
    print("Please,")
print("Can I go to the mall?")
# This will print please 5 times but only once for can I go to the mall.  This is because can I go to the mall is not indented.


for i in range(10):
    print(i)
    # This will print numbers from 0-9

for i in range(1, 11):
    print(i)
    # This will print starting from 1 but it still won't print 11


for i in range(2, 12, 2):
    print(i)
    # The third number is the increment that's being counted by.


for i in [2,6,4,7,8,9]:
    print(i)
    # This will print these numbers specifically.


total = 0
for i in range(5):
    new_number = int(input("Enter a number: "))
    total += new_number
print("The total is: ", total)
# This keeps a running total


sum = 0
for i in range(1, 101):
    sum = sum + i
print(sum)


