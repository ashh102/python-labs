"""
This is my sample drawing program.
"""
# Get the Arcade library.
import arcade

# These go after the import statement.
# Indent 4 spaces afterwards if it doesn't already do it.
def draw_grass():
    """
    This function draws grass.
    """
    arcade.draw_lrtb_rectangle_filled(0, 600, 200, 0, arcade.color.BITTER_LIME)

# Sets the name of window and the dimensions.
arcade.open_window("Drawing Example", 600, 600)

arcade.set_background_color(arcade.color.AIR_SUPERIORITY_BLUE)

arcade.start_render()

# These functions need to go to between the "renders".
draw_grass()

arcade.finish_render()

arcade.run()



# ------------------------

def draw_pine_tree(position_x, position_y)
    """
    This function draws a pine tree
    """

draw_pine_tree(50, 100)
# This will bring the numbers and plug them into the function above in order.

arcade.draw_triangle_filled(position_x - 70, position_y + 60)
# This subtracts or adds the following amounts to the original x and y value that you specified.
