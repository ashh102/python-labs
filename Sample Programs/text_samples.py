# Varibales used in the example "if" statements.
a = 4
b = 5

# Basic comparisons
if a < b:
    print("a is less than b")

if a > b:
    print("a is greater than or equal to b")

print("Done")

# You use 1 = to assign a number to a variable.
# You use 2 == to say that a = b or that some variable is of equal value to another.

# If you want to say "not equal", you can write "a != b".

